;; Copyright (c) 2020-2022 Marek Olejnik <marek.olejnik315@wp.pl>

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ecb-options-version "2.40")
 '(package-selected-packages
   '(ansi-color browse-at-remote company company-c-headers
                company-quickhelp dockerfile-mode dummy-h-mode elpy
                flycheck flycheck-popup-tip go-mode groovy-mode iedit
                impatient-mode ivy lsp-mode lsp-mssql lsp-pyright
                markdown-mode markdown-preview-mode ninja-mode
                omnisharp pos-tip powershell python-mode sqlup-mode
                use-package yasnippet)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(diff-refine-added ((t (:background "#2a602a" :foreground "#f5fff5"))))
 '(diff-refine-removed ((t (:background "#602a2a" :foreground "#fff5f5"))))
 '(lsp-face-semhl-macro ((t (:inherit font-lock-preprocessor-face :foreground "OrangeRed1"))))
 '(magit-diff-added ((t (:extend t :background "#1f401f" :foreground "#f5fff5"))))
 '(magit-diff-added-highlight ((t (:extend t :background "#1f401f" :foreground "#f5fff5"))))
 '(magit-diff-context-highlight ((t (:extend t :background "grey16" :foreground "gray70"))))
 '(magit-diff-file-heading ((t (:extend t :foreground "salmon1" :weight bold))))
 '(magit-diff-removed ((t (:extend t :background "#401f1f" :foreground "#fff5f5"))))
 '(magit-diff-removed-highlight ((t (:extend t :background "#401f1f" :foreground "#fff5f5")))))

;; !!!!! TEMPORARY DISABLED UNTIL IT WILL BE FIXED !!!!!
;;********************************************************************************
;;*********************************  EL-GET PACKAGE MNAGEMENT
;; (add-to-list 'load-path "~/.emacs.d/el-get/el-get")

;; (unless (require 'el-get nil t)
;;   (url-retrieve
;;    "https://github.com/dimitri/el-get/master/el-get-install.el"
;;    (lambda (s)
;;      (end-of-buffer)
;;      (eval-print-last-sexp)))); If there is more than one, they won't work right.

;; ;;install packages on startup
;; (setq my:el-get-packages
;;       '(yasnippet
;;         ))
;; (el-get 'sync my:el-get-packages)
;; (el-get 'sync)


;;********************************************************************************
;;*********************************    MELPA PACKAGE MANAGEMENT
(require 'package)
(setq package-native-compile t)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
  ;; and `package-pinned-packages`. Most users will not need or want to do this.
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  )
(setq package-selected-packages '(
                                  lsp-mode
                                  lsp-ui
                                  lsp-treemacs
                                  lsp-pyright
                                  impatient-mode
                                  yasnippet
                                  python-mode
                                  pyenv-mode
                                  elpy
                                  iedit
                                  pos-tip
                                  company
                                  company-c-headers
                                  company-quickhelp
                                  flycheck
                                  flycheck-popup-tip
                                  magit
                                  markdown-mode
                                  markdown-preview-mode
                                  sqlup-mode
                                  omnisharp
                                  groovy-mode
                                  ansi-color
                                  dockerfile-mode
                                  go-mode
                                  yaml-mode
                                  gitlab-ci-mode
                                  highlight-indent-guides
                                  browse-kill-ring
                                  bash-completion
                                  use-package
                                  projectile
                                  browse-at-remote
                                  ivy
                                  meson-mode
                                  cmake-mode
                                  ninja-mode
                                  typescript-mode
                                  rust-mode
                                  cargo-mode
                                  lua-mode
                                  powershell
                                  csv-mode
                                  ;; mysql
                                  ;; sql-completion
                                  ;; tty-format
                                  ;; rope
                                  )
      )
(package-initialize)
(dolist (p package-selected-packages)
  (unless (package-installed-p p)
    (package-refresh-contents)
    (package-install p)
    )
  )

;;********************************************************************************
;;*********************************  GLOBAL SETTINGS
;; set a default font
(when (member "Droid Sans Mono" (font-family-list))
  (set-face-attribute 'default nil :font "Droid Sans Mono-10"))

(setq visible-bell 1)
(setq-default cursor-type 'bar)

;;kill-ring manipulation
(if (display-graphic-p)
    (progn
      ;; if graphic
      (global-unset-key (kbd "C-z"))
      (global-set-key (kbd "C-z") 'undo)
      )
    ;; else (optional) terminal
    (message "terminal"))
(delete-selection-mode 1)
(global-unset-key (kbd "C-v"))
(global-set-key (kbd "C-v") 'browse-kill-ring)
(setq undo-no-redo t)

;;bash completion
(autoload 'bash-completion-dynamic-complete
  "bash-completion" "BASH completion hook")
(add-hook 'shell-dynamic-complete-functions
          'bash-completion-dynamic-complete)
(setq explicit-bash.exe-args '("--noediting" "--login" "-i"))

;;revert buffer for files changed by other process
(global-auto-revert-mode 1)

;;never use tabs; set tab spaces size
(setq-default indent-tabs-mode nil)
(setq tab-width 4)
(setq c-basic-offset 4)

(global-set-key (kbd "C-a")     'mark-whole-buffer) ; Ctrl+a
;; (global-set-key (kbd "C-x C-a") 'ecb-activate) ; Ctrl+a
;; (setq ecb-tip-of-the-day nil)

;; unset global key F4 and set to previous search result or compile error
(global-unset-key (kbd "<f3>"))
(global-set-key (kbd "<f3>") 'previous-error)

;; unset global key F4 and set to next search result or compile error
(global-unset-key (kbd "<f4>"))
(global-set-key (kbd "<f4>") 'next-error)

(global-set-key (kbd "s-i") 'ispell-region)

(global-set-key (kbd "<S-iso-lefttab>") 'un-indent-by-removing-4-spaces)
(defun un-indent-by-removing-4-spaces ()
  "Remove 4 spaces from beginning of of line."
  (interactive)
  (save-excursion
    (save-match-data
      (beginning-of-line)
      ;; get rid of tabs at beginning of line
      (when (looking-at "^\\s-+")
        (untabify (match-beginning 0) (match-end 0)))
      (when (looking-at "^    ")
        (replace-match "")))))

;;delete whitespace before seve
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;;always apply variables form .dir-locals.el
(setq enable-local-variables :all)

;;kill the current visible buffer without confirmation
;;unless the buffer has been modified
(global-set-key [(control x) (k)] 'kill-current-buffer)

(setq inhibit-startup-screen t)

;;Set focus when opening new window with emacsclient
(add-hook 'server-switch-hook #'raise-frame)
(add-hook 'server-switch-hook (lambda () (select-frame-set-input-focus (selected-frame))))

;;split window 3 ways
;; .-------------.
;; |      |      |
;; |      |      |
;; |      |------|
;; |      |      |
;; |      |      |
;; '-------------'
(defun split-3-ways ()
  "Split window 3-ways.  One main on the left, two on the right."
  (interactive)
  (delete-other-windows)
  (split-window-right)
  (windmove-right)
  (split-window-below)
  (windmove-left)
  (set-frame-parameter nil 'unsplittable t))

(global-set-key (kbd "C-x C-3") 'split-3-ways)
(defun set-unsplittable ()
  "Set windows unsplittable."
  (interactive)
  (set-frame-parameter nil 'unsplittable t))
(global-set-key (kbd "C-x C-1") 'set-unsplittable)

;;windows navigation with M-s and arrows
(global-set-key (kbd "C-s-<right>") 'windmove-right)
(global-set-key (kbd "C-s-<left>") 'windmove-left)
(global-set-key (kbd "C-s-<up>") 'windmove-up)
(global-set-key (kbd "C-s-<down>") 'windmove-down)

;;********************************************************************************
;;*********************************  COLOR THEME
;; Load path with themes
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
;; Load monokai theme on startup
(load-theme 'monokai t)

;;********************************************************************************
;;*********************************  POS_TIP
(with-eval-after-load 'pos-tip
  (setq pos-tip-foreground-color "#F8F8F2")
  (setq pos-tip-background-color "#49483E")
  )

;;********************************************************************************
;;*********************************  DISPLAY LINE NUMBERS
(require 'display-line-numbers)
(set-face-attribute 'line-number-current-line nil :foreground "#BFBFBF")
(set-face-attribute 'line-number-current-line nil :background "#494949")
(set-face-attribute 'line-number nil :background "#2C2C2B")

(defcustom display-line-numbers-exempt-modes
  '(vterm-mode eshell-mode shell-mode term-mode ansi-term-mode treemacs-mode)
  "Major modes on which to disable line numbers."
  :group 'display-line-numbers
  :type 'list
  :version "green")

(defun display-line-numbers--turn-on ()
  "Turn on line numbers except for certain major modes.
Exempt major modes are defined in `display-line-numbers-exempt-modes'."
  (unless (or (minibufferp)
              (member major-mode display-line-numbers-exempt-modes))
    (display-line-numbers-mode)))

(global-display-line-numbers-mode 1)
(column-number-mode 1)

;;********************************************************************************
;;*********************************  HIGHLIGHT INDENT
(with-eval-after-load 'highlight-indent-guides
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-auto-enabled nil)
  (set-face-foreground 'highlight-indent-guides-character-face "grey18")
  )

;;********************************************************************************
;;*********************************  EDIFF
;;do not open control panel in new window
;; (setq ediff-window-setup-function 'ediff-setup-windows-plain)
;; (setq ediff-split-window-function 'split-window-horizontally)

;;********************************************************************************
;;*********************************  EMACS-LISP
(defun add-elisp-mode-hooks ()
  (require 'company)
  ;;  (global-company-mode)
  (company-mode)
  (auto-complete-mode 0)
  (require 'flycheck)
  (flycheck-mode)
  ;;follow symlink if ~/.emacs is a symlink to version controlled file
  (setq vc-follow-symlinks t)
  )
(add-hook 'emacs-lisp-mode-hook 'add-elisp-mode-hooks)

;;********************************************************************************
;;*********************************  PYTHON

(require 'pyenv-mode)

(defun projectile-pyenv-mode-set ()
  "Set pyenv version matching project name."
  (let ((project (projectile-project-name)))
    (if (member project (pyenv-mode-versions))
        (pyenv-mode-set project)
      (pyenv-mode-unset))))

(add-hook 'projectile-after-switch-project-hook 'projectile-pyenv-mode-set)

(defun add-python-mode-hooks ()
  (require 'flycheck)
  (require 'lsp)
  (require 'lsp-pyright)
  (lsp 1)
  (setq lsp-ui-doc-max-width 100)

  ;; Fix flycheck chain of checkers in lsp-mode
  (require 'lsp-diagnostics)
  (set (make-local-variable 'lsp-diagnostic-package) :none)
  (lsp-diagnostics-flycheck-enable)
  (flycheck-select-checker 'python-flake8)

  (setq python3-version-string (replace-regexp-in-string
                                "\n$" ""
                                (shell-command-to-string "python3 --version | cut -d \" \" -f 2")))
  (when (version-list-<=
         (version-to-list "3.10")
         (version-to-list python3-version-string))
    (flycheck-add-next-checker 'python-mypy 'python-pyright))


  (require 'company)
  (setq company-tooltip-maximum-width 100)

  ;;Fix a key binding bug in elpy
  (require 'yasnippet)
  (define-key yas-minor-mode-map (kbd "C-c k") 'yas-expand)
  ;;Fix a key binding bug in iedit
  (require 'iedit)
  (define-key global-map (kbd "C-c o") 'iedit-mode)
  (local-set-key (kbd "RET") 'newline-and-indent)
  (highlight-indentation-mode 0)
  (highlight-indent-guides-mode 1)

  ;;Don't start flycheck when typing (will run when saving)
  (set (make-local-variable 'flycheck-check-syntax-automatically)
       '(save mode-enabled))
  )

;; FIXME: Find out why Python hooks don't work with with-eval-after-load
;; (with-eval-after-load 'python-mode
  (add-hook 'python-mode-hook 'add-python-mode-hooks)

  (require 'flycheck)
  (defvar my-python-command "python3")
  ;; (setq elpy-rpc-python-command my-python-command)
  (setq python-shell-interpreter my-python-command)
  (setq flycheck-python-pycompile-executable my-python-command)
;; )

;;********************************************************************************
;;*********************************  HTML
(with-eval-after-load 'html-mode
  (add-hook 'html-mode-hook
            (lambda ()
              ;; Default indentation is usually 2 spaces, changing to 4.
              (set (make-local-variable 'sgml-basic-offset) 4)))
  )

;;********************************************************************************
;;*********************************  COMPANY
(with-eval-after-load 'company
  (add-hook 'company-mode-hook 'company-quickhelp-mode)
  (add-hook 'company-mode-hook #'(lambda ()
                                   (local-set-key (kbd "C-SPC") 'company-complete)))
  (global-unset-key (kbd "C-n"))
  (global-set-key (kbd "C-n") 'company-complete)
  )

;;********************************************************************************
;;*********************************  FLYCHECK
(with-eval-after-load 'flycheck
  (require 'flycheck-popup-tip)
  (setq flycheck-display-errors-delay 0.4)
  (add-hook 'flycheck-mode-hook 'flycheck-popup-tip-mode)
  )

;;********************************************************************************
;;*********************************  C/C++
(defun add-c-c++-modes-hooks ()
  (require 'cc-vars)
  (require 'yasnippet)
  (yas-global-mode 1)
  (local-set-key (kbd "RET") 'newline-and-indent)
  (local-set-key (kbd "C-c o") 'ff-find-related-file)
  (setq-default indent-tabs-mode nil)
  (require 'lsp)
  (lsp 1)
  ;; Wait a moment after server startup for semantic tokens
  (sleep-for (/ (count-lines (point-min) (point-max)) 50000.0))
  (require 'lsp-semantic-tokens)
  (lsp-semantic-tokens-mode 1)
  (setq compile-command "make -j 10 ")
  (set (make-local-variable 'flycheck-check-syntax-automatically)
       '(save mode-enabled))
  )
(add-hook 'c++-mode-hook 'add-c-c++-modes-hooks)
(add-hook 'c-mode-hook 'add-c-c++-modes-hooks)
(add-hook 'vala-mode-hook 'add-c-c++-modes-hooks)

(defconst my-c-lineup-maximum-indent 30)
(defun my-c-lineup-arglist (langelem)
  (let ((ret (c-lineup-arglist langelem)))
    (if (< (elt ret 0) my-c-lineup-maximum-indent)
        ret
      (save-excursion
        (goto-char (cdr langelem))
        (vector (+ (current-column) (* 2 tab-width)))))))

(defun my-set-tab-size (s)
  (setq tab-width s)
  (setq c-basic-offset s)
  )

(defun my-default-c-c++-indent ()
  (my-set-tab-size 4)
  (setq c-default-style "ellemtel")
  (setcdr (assoc 'arglist-cont-nonempty c-offsets-alist)
          '(c-lineup-gcc-asm-reg my-c-lineup-arglist))
  )

(defun my-c-gstreamer-indent ()
  (my-set-tab-size 2)
  (setq c-default-style "ellemtel")
  (setcdr (assoc 'arglist-cont-nonempty c-offsets-alist)
          '(c-lineup-gcc-asm-reg my-c-lineup-arglist))
  (add-to-list 'c-cleanup-list 'space-before-funcall)
  (c-set-offset 'substatement-open '0)
  (c-set-offset 'label -1000)
  (c-set-offset 'case-label '+)
  (c-set-offset 'statement-cont (+ 2 tab-width))
  (c-set-offset 'arglist-cont-nonempty (+ 2 tab-width))

  ;;Highlight over 80 characters in column
  (require 'whitespace)
  (setq whitespace-line-column 80) ;; limit line length
  (setq whitespace-style '(face lines-tail))
  (setq whitespace-line 'font-lock-warning-face)
  (whitespace-mode)
  )

(add-hook 'c++-mode-hook 'my-default-c-c++-indent)
(add-hook 'c-mode-hook 'my-c-gstreamer-indent)
(add-hook 'vala-mode-hook 'my-default-c-c++-indent)

(when (not (require 'dummy-h-mode nil :noerror))
  (let ((dummy-h-tmp-path (file-name-concat (temporary-file-directory) "dummy-h-mode.el")))
    (url-copy-file
     "https://raw.githubusercontent.com/emacsattic/dummy-h-mode/master/dummy-h-mode.el"
     dummy-h-tmp-path t)
    (package-install-file dummy-h-tmp-path)
    )
  )
(add-to-list 'auto-mode-alist '("\\.h$" . dummy-h-mode))

;;********************************************************************************
;;*********************************  C#
(defun add-csharp-mode-hooks ()
  (require 'company)
  (require 'omnisharp)
  (require 'flycheck)
  (require 'yasnippet)
  (require 'lsp)
  (lsp 1)
  )
(add-hook 'csharp-mode-hook 'add-csharp-mode-hooks)

;;********************************************************************************
;;*********************************  SQL
;; (require 'sql-completion)
;; (setq sql-interactive-mode-hook
;;       (lambda ()
;;         (define-key sql-interactive-mode-map "\t" 'comint-dynamic-complete)
;;         (sql-mysql-completion-init)))

(when (require 'sql-upcase nil :noerror)
  (add-hook 'sql-mode-hook 'sql-upcase-mode)
  (add-hook 'sql-interactive-mode-hook 'sql-upcase-mode)
  )
;; (require 'mysql)
;; (if (eq system-type 'windows-nt)
;;     (progn
;;       (setq sql-mysql-program "C:/Users/molejnik/Downloads/mysql-5.7.14-winx64/bin/mysql")
;;       (setq sql-mysql-options '("-C" "-t" "-f" "-n" "--prompt=mysql> "))
;;       )
;;   )

;;********************************************************************************
;;*********************************  GROOVY
(defun add-groovy-mode-hooks ()
  ;; Indent
  (my-set-tab-size 4)

  (require 'whitespace)
  (setq-local whitespace-line-column 120) ;; limit line length
  (setq-local whitespace-style '(face lines-tail))
  (setq-local whitespace-line 'font-lock-warning-face)
  (whitespace-mode)
  (require 'compile)
  (set (make-local-variable 'compile-command)
       (format "npm-groovy-lint --files \"%s\" -l \"warning\" 2>/dev/null" buffer-file-name))
  (require 'lsp)
  (lsp 1)
  ;;Don't start flycheck when typing (will run when saving)
  (set (make-local-variable 'flycheck-check-syntax-automatically)
       '(save mode-enabled))
  )
(with-eval-after-load 'groovy-mode
  (add-hook 'groovy-mode-hook 'add-groovy-mode-hooks)
  (require 'lsp)
  ;; https://github.com/prominic/groovy-language-server
  (setq lsp-groovy-server-file "~/.emacs.d/groovy-language-server-all.jar")
  (if (and (executable-find "javacc") (not (file-exists-p lsp-groovy-server-file)))
      (progn
        (setq-local groovy-ls-tmp-dir "/tmp/groovy-language-server")
        (shell-command (format "git clone --depth 1 https://github.com/prominic/groovy-language-server %s" groovy-ls-tmp-dir))
        (let ((default-directory groovy-ls-tmp-dir))
          (shell-command "./gradlew build"))
        (copy-file (concat groovy-ls-tmp-dir "/build/libs/groovy-language-server-all.jar") lsp-groovy-server-file)
        (delete-directory groovy-ls-tmp-dir t)))
  (setq groovy-indent-offset 4)
  )
(add-to-list 'auto-mode-alist '("\\Jenkinsfile\\'" . groovy-mode))

;;********************************************************************************
;;*********************************  SH-MODE
(defun add-sh-mode-hooks ()
  ;; Indent
  ;; (my-set-tab-size 2)
  ;; (setq sh-basic-offset 2)
  ;; (setq sh-indentation-offset 2)

  (setq lsp-ui-doc-max-height 40)
  (require 'lsp)
  (require 'lsp-ui)
  (lsp 1)

  (require 'flycheck)

  ;; Fix flycheck chain of checkers in lsp-mode
  (require 'lsp-diagnostics)
  (lsp-diagnostics-flycheck-enable)
  (flycheck-add-next-checker 'lsp 'sh-bash)

  (highlight-indent-guides-mode 1)
  )

(add-hook 'sh-mode-hook 'add-sh-mode-hooks)

;;********************************************************************************
;;*********************************  MARKDOWN
(defun markdown-disable-whitespace-removal ()
  (remove-hook 'before-save-hook 'delete-trailing-whitespace)
  )
(add-hook 'markdown-mode-hook 'markdown-disable-whitespace-removal)


(require 'ansi-color)
(defun display-ansi-colors ()
  (interactive)
  (format-decode-buffer 'ansi-colors))


;;********************************************************************************
;;*********************************  GO
(defun gofmt-on-save ()
  (shell-command-to-string (format "gofmt -w %s" buffer-file-name))
  )

(defun add-go-mode-hooks ()
  (my-set-tab-size 4)
  (add-hook 'before-save-hook 'gofmt-before-save)
  (require 'lsp)
  (lsp 1)
  )
(add-hook 'go-mode-hook 'add-go-mode-hooks)

;;********************************************************************************
;;*********************************  LSP
(with-eval-after-load 'lsp
  (require 'lsp-mode))
(with-eval-after-load 'lsp-mode
  (require 'company)
  (setq gc-cons-threshold (* 100 1024 1024)
        read-process-output-max (* 1024 1024)
        company-idle-delay 0.0
        company-minimum-prefix-length 1
        lsp-idle-delay 0.1)
  (require 'lsp-treemacs)
  (add-hook 'lsp-treemacs-after-jump-hook
            (lambda () (kill-buffer lsp-treemacs-symbols-buffer-name)))
  (setq lsp-treemacs-sync-mode 1)

  (require 'lsp-ui)
  (require 'lsp-ui-doc)
  (setq lsp-ui-doc-show-with-mouse nil)
  (setq lsp-ui-doc-position 'at-point)
  (setq lsp-ui-sideline-show-diagnostics nil)
  (local-set-key (kbd "<f1>") 'ui-set-show-hide)
  (local-set-key (kbd "C-c r") 'lsp-rename)
  (setq lsp-file-watch-threshold 10000)
  )
(setq lsp-keymap-prefix "C-l")

(defvar ui-show-hide-switch t)
(defun ui-set-show-hide ()
  (interactive)
  (if ui-show-hide-switch
      (progn
        (lsp-ui-doc-show)
        (setq ui-show-hide-switch nil))
    (progn
      (lsp-ui-doc-hide)
      (setq ui-show-hide-switch t)))
  )

;;********************************************************************************
;;*********************************  GITLAB
(defun my-file-contents (filename)
  "Return the contents of FILENAME."
  (with-temp-buffer
    (insert-file-contents filename)
    (buffer-string)))

(defun add-gitlab-ci-mode-hooks ()
  (require 'highlight-indent-guides)
  (highlight-indent-guides-mode 1)
  (require 'company)
  (company-mode 1 )
  )
(with-eval-after-load 'gitlab-ci-mode
  (if (file-exists-p "~/.gitlab-api-token")
      (setq gitlab-ci-api-token (replace-regexp-in-string "\n$" "" (my-file-contents "~/.gitlab-api-token"))))
  (add-hook 'gitlab-ci-mode-hook 'add-gitlab-ci-mode-hooks)
  )

;;********************************************************************************
;;*********************************  MAGIT
(require 'magit)
(with-eval-after-load 'magit
  ;;hithlight word changes lines
  (setq magit-diff-refine-hunk (quote all))
  )

;;********************************************************************************
;;*********************************  DOCKERFILE
(defun add-dockerfile-mode-hooks ()
  (require 'lsp)
  (lsp 1)
  )
(with-eval-after-load 'dockerfile-mode
 (add-hook 'dockerfile-mode-hook 'add-dockerfile-mode-hooks)
  )
(add-to-list 'auto-mode-alist '("\\Dockerfile*" . dockerfile-mode))


;;********************************************************************************
;;*********************************  MESON
(defun add-meson-mode-hooks ()
  (require 'company)
  (company-mode)
  )
(with-eval-after-load 'meson-mode
 (add-hook 'meson-mode-hook 'add-meson-mode-hooks)
 )

;;********************************************************************************
;;*********************************  CMAKE
(defun add-cmake-mode-hooks ()
  (require 'lsp)
  (lsp 1)
  )
(with-eval-after-load 'cmake-mode
 (add-hook 'cmake-mode-hook 'add-cmake-mode-hooks)
 )
(add-to-list 'auto-mode-alist '("\\.cmake$" . cmake-mode))

;;********************************************************************************
;;*********************************  TYPESCRIPT
(defun add-typescript-mode-hooks ()
  (require 'lsp)
  (lsp 1)
  (highlight-indent-guides-mode 1)
  (setq-default typescript-indent-level 2)

    ;;Don't start flycheck when typing (will run when saving)
  (set (make-local-variable 'flycheck-check-syntax-automatically)
       '(save mode-enabled))
  )
(with-eval-after-load 'typescript-mode
 (add-hook 'typescript-mode-hook 'add-typescript-mode-hooks)
  )
(add-to-list 'auto-mode-alist '("\\.tsx$" . typescript-mode))

;;********************************************************************************
;;*********************************  CSS, SCSS
(defun add-css-mode-hooks ()
  (require 'lsp)
  (lsp 1)

  ;;Don't start flycheck when typing (will run when saving)
  (set (make-local-variable 'flycheck-check-syntax-automatically)
       '(save mode-enabled))
  )
(with-eval-after-load 'css-mode
  (add-hook 'css-mode-hook 'add-css-mode-hooks))
(with-eval-after-load 'scss-mode
  (add-hook 'scss-mode-hook 'add-css-mode-hooks))

;;********************************************************************************
;;*********************************  IONIC
(require 'projectile)
(projectile-register-project-type 'ionic '("ionic.config.json")
                                  :project-file "ionic.config.json"
				  :compile "ionic serve"
				  :test "ionic serve"
                                  )

;;********************************************************************************
;;*********************************  RUST
(add-to-list 'exec-path "~/.cargo/bin")
(defun add-rust-mode-hooks ()
  (require 'lsp)

  ;; (setq-default lsp-rust-analyzer-display-closure-return-type-hints 1)
  ;; (setq-default lsp-rust-analyzer-binding-mode-hints 1)
  ;; (setq-default lsp-rust-analyzer-display-chaining-hints 1)
  ;; (setq-default lsp-rust-analyzer-display-parameter-hints 1)
  (setq-default lsp-inlay-hint-enable t)
  (lsp 1)
  (lsp-inlay-hints-mode 1)
  (yas-minor-mode 1)
  ;;Don't start flycheck when typing (will run when saving)
  (set (make-local-variable 'flycheck-check-syntax-automatically)
       '(save mode-enabled))
  )
(with-eval-after-load 'rust-mode
  (add-hook 'rust-mode-hook 'add-rust-mode-hooks)
  )

;;********************************************************************************
;;*********************************  LUA
(require 'lua-mode)
(defun add-lua-mode-hooks ()
  (require 'lsp)
  (setq lsp-lua-workspace-preload-file-size 300)
  (lsp 1)
  )
(with-eval-after-load 'lua-mode
 (add-hook 'lua-mode-hook 'add-lua-mode-hooks)
  )

;;********************************************************************************
;;*********************************  POWERSHELL
(require 'powershell)
(defun add-powershell-mode-hooks ()
  (require 'lsp)
  (lsp 1)
  )
(with-eval-after-load 'powershell
 (add-hook 'powershell-mode-hook 'add-powershell-mode-hooks)
  )

;;********************************************************************************
;;*********************************  CSV
(require 'csv-mode)
(defun add-csv-mode-hooks ()
  (csv-align-mode 1)
  )
(with-eval-after-load 'powershell
  (add-hook 'csv-mode-hook 'add-csv-mode-hooks)
  )

;;********************************************************************************
;;*********************************  fluendo-cerbero specific
(use-package fluendo
  :load-path "~/.emacs.d/config")

;;********************************************************************************
;;*********************************  PROJECTILE/PROJECT
(require 'projectile)
(require 'browse-at-remote)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(global-set-key (kbd "C-c f") 'projectile-find-file)
(projectile-mode)
(setq projectile-completion-system 'ivy)
(global-set-key (kbd "C-f") 'project-find-regexp)
(global-set-key (kbd "C-c t") 'treemacs)
(setq treemacs-default-visit-action 'treemacs-visit-node-close-treemacs)

;;Compilation
;;Tap <f5> or <f6> key to run last compilation command
;;To change command tap C-u and then <f5>, now you can enter new compilation command
;;In C/C++ you can compile in specified directory use -C option, example make -C ./build/
(global-set-key (kbd "<f5>") (lambda ()
                               (interactive)
                               (setq-local compilation-read-command nil)
                               (call-interactively 'projectile-compile-project)))
(global-set-key (kbd "<f6>") (lambda ()
                               (interactive)
                               (setq-local compilation-read-command nil)
                               (call-interactively 'compile)))
(defvar default-compile-command "make -j $(nproc) ")
(setq compile-command default-compile-command)

;;********************************************************************************


(defun disable-flycheck-in-scratch-buffer ()
  "Disable flycheck mode in the *scratch* buffer."
  (when (string-equal (buffer-name) "*scratch*")
    (flycheck-mode -1)))
(add-hook 'lisp-interaction-mode-hook 'disable-flycheck-in-scratch-buffer)


(message "FULL CONFIG LOADED")
