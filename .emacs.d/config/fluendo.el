;;; fluendo.el --- Fluendo specific configuration -*- lexical-binding: t; -*-

(add-to-list 'auto-mode-alist '("\\.recipe\\'" . python-mode))
(add-to-list 'auto-mode-alist '("\\.package\\'" . python-mode))
(add-to-list 'auto-mode-alist '("\\.cbc\\'" . python-mode))

(require 'projectile)
(projectile-register-project-type 'flu-plugins '("gst" "libs" "ci" "scripts" "tests")
                                  :project-file "meson_options.txt"
				  :compile "ninja -C builddir -j8"
				  :test "meson test -C builddir"
                                  )

(provide 'fluendo)
