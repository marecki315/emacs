# Emacs multi IDE #

-----------------------

## Build from source ##

Go tu `build` directory and run `make` to list build options.
Package will be built in docker and then copied to local folder:

    $ make emacs.package.focal.native
     ...
    $ ls focal-native/emacs-packages/
    emacs-mo-build_28.0.50_amd64.deb

Use `emacs.package.focal.native.nox` for NOX build

> :warning: Dockerfiles for Xenial and Bionic might be outdated


## Download and install lates build ##

From time to time, emacs build from latest master is run in GitLab
pipline and can be downloaded.

Ubuntu Focal:  
Standard: https://gitlab.com/api/v4/projects/marecki315%2Femacs/jobs/artifacts/build-package-focal/download?job=build-emacs-focal  
NOX: https://gitlab.com/api/v4/projects/marecki315%2Femacs/jobs/artifacts/build-package-focal-nox/download?job=build-emacs-focal-nox

Ubuntu Jammy:  
Standard: https://gitlab.com/api/v4/projects/marecki315%2Femacs/jobs/artifacts/build-package-jammy/download?job=build-emacs-jammy

Download and install:
    
    $ curl -L --output artifacts.zip https://gitlab.com/api/v4/projects/marecki315%2Femacs/jobs/artifacts/build-package-focal/download?job=build-emacs-focal # or other url
    $ unzip artifacts.zip
    $ find . -name "emacs*.deb" | xargs -I {} sudo apt install -f -y {}

#### Install and setup environment ####

Check [install.sh](install.sh)


## FAQ ##

##### Theme doesn't load in terminal? #####

    $ export TERM=xterm-256color  # echo "export TERM=xterm-256color" >> ~/.bashrc
    $ export COLORTERM=truecolor  # echo "export COLORTERM=truecolor" >> ~/.bashrc
 
    
