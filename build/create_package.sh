#!/bin/bash

# shellcheck disable=SC2086

set -ue

path_to_absolute() {
  if [[ $1 != /* ]]; then
    echo "$(pwd)/$1"
  else
    echo $1
  fi
}


PROJECT_NAME=$1
VERSION=$2
PREFIX_DIR=$(path_to_absolute $3)
OUT_DIR=$(path_to_absolute ${4:-"."})
SCRIPT_DIR=$(pwd)
TMP_DIR=$(mktemp -d)

PACKAGE_DIR=$TMP_DIR/$PROJECT_NAME-$VERSION

echo $TMP_DIR

mkdir $PACKAGE_DIR
# Create source tree in TMP_DIR
cp -r $PREFIX_DIR/* $PACKAGE_DIR/
echo $PREFIX_DIR
ls $PREFIX_DIR
echo ----------------
echo $PACKAGE_DIR
ls $PACKAGE_DIR/

mkdir $PACKAGE_DIR/debian
cp $SCRIPT_DIR/debian/* $PACKAGE_DIR/debian/

sed -i "s/28.0.50/${VERSION}/g" $PACKAGE_DIR/debian/changelog
for f in $PACKAGE_DIR/debian/changelog $PACKAGE_DIR/debian/control; do
    sed -i "s/emacs-mo-build/${PROJECT_NAME}/g" ${f}
done


touch $PACKAGE_DIR/debian/$PROJECT_NAME.install
mkdir $PACKAGE_DIR/debian/source
echo "3.0 (native)" > $PACKAGE_DIR/debian/source/format

cd $PACKAGE_DIR

# Prepare list of files to install
array=()
while IFS=  read -r -d $'\0'; do
    array+=("$REPLY")
done < <(find ./usr -type f -print0)
while IFS=  read -r -d $'\0'; do
    array+=("$REPLY")
done < <(find ./usr -type l -print0)

for f in ${array[@]}
do
  echo ${f/\.\//} $(dirname ${f/\.\//}) >> $PACKAGE_DIR/debian/$PROJECT_NAME.install
done

dpkg-buildpackage -rfakeroot -us -uc -b #FIXME -ui

cp ../${PROJECT_NAME}_${VERSION}_amd64.deb $OUT_DIR/

if [ -d "$TMP_DIR" ]; then rm -Rf $TMP_DIR; fi
