#!/bin/bash

# shellcheck disable=SC2086

set -ue

path_to_absolute() {
  if [[ $1 != /* ]]; then
    echo "$(pwd)/$1"
  else
    echo $1
  fi
}

EMACS_DIR=$(path_to_absolute $1)
PREFIX_DIR=$(path_to_absolute $2)
OUT_DIR=$(path_to_absolute ${3:-"."})
SCRIPTPATH=$( cd "$(dirname "$0")" ; pwd -P )
PROJECT_NAME=${4:-"emacs-mo-build"}

EMACS_VERSION=$(cat $EMACS_DIR/src/Makefile | grep 'version = ' | cut -d' ' -f3)

$SCRIPTPATH/create_package.sh $PROJECT_NAME $EMACS_VERSION $PREFIX_DIR $OUT_DIR
