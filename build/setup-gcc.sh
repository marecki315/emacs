#!/bin/bash

set -xue

sudo apt-get install -y lsb-release

readonly SYSTEM_CODENAME=$(lsb_release -a 2>&1 | grep Codename | cut -f2)

if [[ $SYSTEM_CODENAME == "focal" ]]; then
    readonly GCC_VERSION=10
    readonly LIBGCCJIT_VERSION=$((GCC_VERSION-1))
fi
if [[ $SYSTEM_CODENAME == "jammy" ]]; then
    readonly GCC_VERSION=12
    readonly LIBGCCJIT_VERSION=$((GCC_VERSION-1))
fi
if [[ $SYSTEM_CODENAME == "noble" ]]; then
    readonly GCC_VERSION=14
    readonly LIBGCCJIT_VERSION=$((GCC_VERSION-1))
fi

# GCC
sudo apt-get update -y && sudo apt-get install -y \
                               gcc-${GCC_VERSION} \
                               g++-${GCC_VERSION} \
                               libgccjit-${LIBGCCJIT_VERSION}-dev

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-${GCC_VERSION} 800 \
     --slave /usr/bin/g++ g++ /usr/bin/g++-${GCC_VERSION}
