#!/bin/bash

# shellcheck disable=SC2038,SC2086

set -xe

FORCE_REINSTALL=false
MAKE_BUILD=false

# Parse options
while getopts "fb" opt; do
    case $opt in
        f)
            FORCE_REINSTALL=true
            ;;
        b)
            MAKE_BUILD=true
            ;;
        *)
            echo "Usage: $0 [-f] [-b]"
            exit 1
            ;;
    esac
done

# Check for force reinstall (-f)
if ! $FORCE_REINSTALL; then
    if test -f ~/.emacs; then
        echo "Emacs already installed, use -f to force reinstall"
        exit 0
    fi
fi

set -u

sudo apt-get install -y lsb-release curl unzip

readonly SCRIPTPATH=$( cd "$(dirname "$0")" ; pwd -P )
readonly TMP_DIR=/tmp/emacs-bins
readonly ARTIFACTS_ZIP=${TMP_DIR}/artifacts.zip
readonly ALIASES_FILE=~/.bash_aliases
readonly SYSTEM_CODENAME=$(lsb_release -a 2>&1 | grep Codename | cut -f2)
PIP_EXTRA_ARGS=""

# Download and install binary
rm -rf ${TMP_DIR} && mkdir ${TMP_DIR}
# Check for make build
if $MAKE_BUILD; then
    readonly MAKE_DIR="${SCRIPTPATH}/build"
    make -C ${MAKE_DIR} emacs.package.${SYSTEM_CODENAME}.native
    cp ${MAKE_DIR}/${SYSTEM_CODENAME}-native/emacs-packages/* ${TMP_DIR}
    find ${TMP_DIR} -name "emacs*.deb" | xargs -I {} echo {}
else
    curl -L --output ${ARTIFACTS_ZIP} https://gitlab.com/api/v4/projects/marecki315%2Femacs/jobs/artifacts/build-package-${SYSTEM_CODENAME}/download?job=build-emacs-${SYSTEM_CODENAME}
    unzip ${ARTIFACTS_ZIP} -d ${TMP_DIR}
fi

sudo apt remove emacs-mo-build -y || true
sudo apt remove emacs emacs2* -y || true
find ${TMP_DIR} -name "emacs*.deb" | xargs -I {} sudo apt install -f -y {}

# Copy configuration (create symlinks to files in the repo)
rm -rf ~/.emacs ~/.emacs.d
ln -s ${SCRIPTPATH}/.emacs ~/.emacs
cd ./.emacs.d
find . -name "*.el" | xargs -I {} dirname {} | xargs -I {} mkdir ~/.emacs.d/{} -p
find . -name "*.el" | xargs -I {} ln -s ${PWD}/{} ~/.emacs.d/{}
cd ${SCRIPTPATH}

# Add aliases
if ! test -f ${ALIASES_FILE}; then touch ${ALIASES_FILE} ; fi
if ! grep -Fxq -v "alias ec=" ${ALIASES_FILE}; then
    echo "alias ec='emacsclient -c'" >> ${ALIASES_FILE}
    echo "alias emacs='emacsclient -c'" >> ${ALIASES_FILE}
    echo "alias emacs_kill-daemon='emacsclient --eval \"(kill-emacs)\"'" >> ${ALIASES_FILE}
else
    echo 1
fi


isDocker(){
    local cgroup=/proc/1/cgroup
    test -f $cgroup && [[ "$(<$cgroup)" = *:cpuset:/docker/* ]]
}

isDockerBuildkit(){
    local cgroup=/proc/1/cgroup
    test -f $cgroup && [[ "$(<$cgroup)" = *:cpuset:/docker/buildkit/* ]]
}

isDockerContainer(){
    [ -e /.dockerenv ]
}

isAnyDockerType(){
    isDockerBuildkit || isDocker || isDockerContainer
}

if ! isAnyDockerType; then
    # Setup daemon as service
    systemctl stop --user emacs
    systemctl disable --user emacs
    mkdir -p ~/.config/systemd/user/
    cp ${SCRIPTPATH}/emacs.service ~/.config/systemd/user/emacs.service
    systemctl enable --user emacs
    emacs --script ~/.emacs
    systemctl start --user emacs
fi

# Install language servers and tools
if command -v npm &> /dev/null; then
    sudo npm install -g \
         bash-language-server \
         dockerfile-language-server-nodejs \
         npm-groovy-lint \
         yaml-language-server \
         typescript-language-server \
         typescript \
         vscode-langservers-extracted
fi
if command -v pip3 &> /dev/null; then
    if [[ $SYSTEM_CODENAME == "noble" ]]; then
        PIP_EXTRA_ARGS=" --break-system-packages "
    fi
    sudo pip3 install pyright cmake-language-server ${PIP_EXTRA_ARGS}
fi
